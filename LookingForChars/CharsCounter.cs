﻿using System;

namespace LookingForChars
{
    public static class CharsCounter
    {
        public static int GetCharsCount(string str, char[] chars)
        {
            if (str == null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars == null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            int count = 0;

            for (int i = 0; i < str.Length; i++)
            {
                for (int j = 0; j < chars.Length; j++)
                {
                    if (chars[j] == str[i])
                    {
                        count++;
                        break;
                    }
                }
            }

            return count;
        }

        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex)
        {
            if (str == null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars == null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (startIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (endIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            if (endIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            if (endIndex < startIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            int count = 0, i = startIndex, j;

            while (i <= endIndex)
            {
                j = 0;
                while (j < chars.Length)
                {
                    if (chars[j] == str[i])
                    {
                        count++;
                        break;
                    }

                    j++;
                }

                i++;
            }

            return count;
        }

        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex, int limit)
        {
            if (str == null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars == null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (startIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (endIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            if (endIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            if (endIndex < startIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            if (limit < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(limit));
            }

            int count = 0, i = startIndex, j;

            do
            {
                j = 0;

                do
                {
                    if (str[i] == chars[j])
                    {
                        count++;
                        break;
                    }

                    j++;
                }
                while (j < chars.Length);

                i++;
            }
            while (count < limit && i <= endIndex);

            return count;
        }
    }
}
